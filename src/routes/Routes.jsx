import { useAuthContext } from '@/context';
import { Navigate, Route, Routes } from 'react-router-dom';
import {
  AdminLayout,
  AuthLayout,
  ClientLayout,
  DefaultLayout
} from '../layouts';
import {
  allAdminFlattedRoutes,
  allAuthFlattedRoutes,
  allBlankFlattedRoutes,
  allSuperAdminFlattedRoutes,
  clientProtectedFlattedRoutes,
  clientPublicFlattedRoutes
} from './index';

const AllRoutes = (props) => {
  const { session, isAuthenticated } = useAuthContext();
  console.log({ session, isAuthenticated });
  return (
    <Routes>
      <Route>
        {allBlankFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={<DefaultLayout {...props}>{route.element}</DefaultLayout>}
          />
        ))}
      </Route>

      <Route>
        {allAuthFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={<AuthLayout {...props}>{route.element}</AuthLayout>}
          />
        ))}
      </Route>

      <Route>
        {clientPublicFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={<ClientLayout {...props}>{route.element}</ClientLayout>}
          />
        ))}
      </Route>

      <Route>
        {clientProtectedFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={
              isAuthenticated && session?.role == 'user' ? (
                <ClientLayout {...props}>{route.element}</ClientLayout>
              ) : (
                <Navigate
                  to={{
                    pathname: '/auth/login',
                    search: 'redirectTo=' + route.path
                  }}
                />
              )
            }
          />
        ))}
      </Route>

      <Route>
        {allAdminFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={
              isAuthenticated && session?.role == 'admin' ? (
                <AdminLayout {...props}>{route.element}</AdminLayout>
              ) : (
                <Navigate
                  to={{
                    pathname: '/auth/login',
                    search: 'redirectTo=' + route.path
                  }}
                />
              )
            }
          />
        ))}
      </Route>
      <Route>
        {allSuperAdminFlattedRoutes.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            element={
              isAuthenticated && session?.role == 'superAdmin' ? (
                <AdminLayout {...props}>{route.element}</AdminLayout>
              ) : (
                <Navigate
                  to={{
                    pathname: '/auth/login',
                    search: 'redirectTo=' + route.path
                  }}
                />
              )
            }
          />
        ))}
      </Route>
    </Routes>
  );
};

export default AllRoutes;
