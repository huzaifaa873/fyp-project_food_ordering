import { AuthProvider, FilterProvider, LayoutProvider } from '@/context';
import { lazy, useEffect } from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { store } from '../redux/store';
const ShopProvider = lazy(() => import('@/context/useShoppingContext'));

const AppProvidersWrapper = ({ children }) => {
  const handleChangeTitle = () => {
    if (document.visibilityState == 'hidden')
      document.title = 'Please come back :-(';
    else
      document.title = 'Yum Reactjs - Multipurpose Food Tailwind CSS Template';
  };

  useEffect(() => {
    import('preline');

    document.addEventListener('visibilitychange', handleChangeTitle);
    return () => {
      document.removeEventListener('visibilitychange', handleChangeTitle);
    };
  }, []);

  return (
    <Provider store={store}>
      <HelmetProvider>
        <LayoutProvider>
          <AuthProvider>
            <ShopProvider>
              <FilterProvider>{children}</FilterProvider>
            </ShopProvider>
          </AuthProvider>
        </LayoutProvider>
      </HelmetProvider>
    </Provider>
  );
};
export default AppProvidersWrapper;
