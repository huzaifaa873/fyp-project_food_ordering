import {
  LuEye,
  LuMail,
  LuMapPin,
  LuPencil,
  LuPhone,
  LuTrash2
} from 'react-icons/lu';
import { Link, useNavigate } from 'react-router-dom';
import { useDelResturant } from '../../helpers/api';

const RestaurantListCard = ({ restaurant }) => {
  console.log(restaurant);
  const { contact_no, email, logo, total_dishes, total_sales, name } =
    restaurant;
  const address = restaurant?.city;
  const restaurantId = restaurant?._id;
  const { mutate: triggerRestDelete } = useDelResturant();
  // useEffect(() => {
  //   (async () => {
  //     const foundSeller = await getSellerById(restaurantId);
  //     if (foundSeller) setSeller(foundSeller);
  //   })();
  // }, [restaurantId]);
  //
  const navigate = useNavigate();
  return (
    <div className='relative rounded-lg border border-default-200 p-6'>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          gap: '0.5rem'
        }}
      >
        {/* Edit  */}
        <LuPencil
          size={20}
          style={{ cursor: 'pointer' }}
          onClick={() => {
            navigate(`/edit-restaurant?restaurant=${restaurantId}`);
          }}
        />
        {/* View  */}
        <LuEye
          size={20}
          style={{ cursor: 'pointer' }}
          onClick={() => {
            navigate(`/restaurants/${restaurantId}`);
          }}
        />
        {/* Delete  */}
        <LuTrash2
          onClick={() => {
            triggerRestDelete(restaurantId);
          }}
          style={{ cursor: 'pointer' }}
          size={20}
        />
        {/* <IoMdAddCircleOutline
          style={{ cursor: "pointer" }}
          size={20}
          onClick={() => {
            navigate(`/add-restaurant?restaurant=${restaurantId}`);
          }}
        /> */}
      </div>
      <img
        src={`${restaurant?.image}`}
        style={{
          objectFit: 'cover',
          objectPosition: 'center',
          width: '100%',
          height: '180px',
          marginTop: '1rem'
        }}
        width={56}
        height={56}
        className='mx-auto mb-4 h-14 w-14'
        alt='restaurant'
      />

      <h4 className='mb-0 ml-[28px] text-left text-base font-medium text-default-600'>
        {restaurant?.businessName}
      </h4>
      <div className='mb-8 flex justify-around'>
        <div className='text-center'>
          <h4 className='mb-2.5 text-lg font-medium text-primary'>
            {total_dishes}
          </h4>
          <h5 className='text-sm text-default-800'>Total Product</h5>
        </div>
        <div className='border-s border-default-200' />
        <div className='text-center'>
          <h4 className='mb-2.5 text-lg font-medium text-primary'>
            {total_sales}
          </h4>
          <h5 className='text-sm text-default-800'>Total Sales</h5>
        </div>
      </div>
      <div className='mb-6 space-y-5'>
        <div className='flex gap-3'>
          <div className='flex-shrink'>
            <LuMapPin size={20} className='text-default-800' />
          </div>
          <p className='d text-sm text-default-700'>{address}</p>
        </div>
        <div className='flex gap-3'>
          <div className='flex-shrink'>
            <LuMail size={20} className='text-default-800' />
          </div>
          <p className='d text-sm text-default-700'>{email}</p>
        </div>
        <div className='flex gap-3'>
          <div className='flex-shrink'>
            <LuPhone size={20} className='text-default-800' />
          </div>
          <p className='d text-sm text-default-700'>{contact_no}</p>
        </div>
      </div>
      <div className='text-center'>
        <Link
          to={`/restaurants/${restaurantId}`}
          className='inline-flex rounded-lg bg-primary px-8 py-2.5 font-medium text-white transition-all hover:bg-primary-500'
        >
          View Details
        </Link>
      </div>
    </div>
  );
};

export default RestaurantListCard;
