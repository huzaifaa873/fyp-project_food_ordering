import {
  SelectFormInput,
  TextAreaFormInput,
  TextFormInput
} from '@/components';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { LuEraser, LuSave } from 'react-icons/lu';
import ReactQuill from 'react-quill';
import * as yup from 'yup';

//style
import { useEffect, useState } from 'react';
import 'react-quill/dist/quill.snow.css';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'sonner';
import { useCreateDish } from '../../../../helpers/api';
import DishUploader from './DishUploader';

const AddDishForm = () => {
  const [files, setFiles] = useState([]);
  const [base64, setBase64] = useState('');
  const [finalObj, setFinalObj] = useState({});
  const location = useLocation();
  const navigate = useNavigate();
  const query = new URLSearchParams(location.search);
  let valueSnow = '';
  valueSnow = `<h5><span class="ql-size-large">Add a long description for your product</span></h5>`;

  const credentialsManagementFormSchema = yup.object({
    productname: yup.string().required('Please enter your product name'),
    productCatagory: yup
      .mixed()
      .test(
        'is-object',
        'productCatagory must be an object with string values',
        (value) => {
          if (!value || typeof value !== 'object') return false;
          return (
            typeof value.label === 'string' && typeof value.value === 'string'
          );
        }
      )
      .required('Please select your productCatagory.'),
    sellingPrice: yup.number().required('Please enter your selling price'),
    costPrice: yup.number().required('Please enter your selling price'),
    quantity: yup.number().required('Please enter your quantity'),
    deliveryType: yup
      .mixed()
      .test(
        'is-object',
        'deliveryType must be an object with string values',
        (value) => {
          if (!value || typeof value !== 'object') return false;
          return (
            typeof value.label === 'string' && typeof value.value === 'string'
          );
        }
      )
      .required('Please select your deliveryType.'),
    description: yup.string().required('Please enter your description')
  });

  const { control, handleSubmit, reset, watch } = useForm({
    resolver: yupResolver(credentialsManagementFormSchema)
  });

  const productNameValue = watch('productname');
  const productCategoryValue = watch('productCategory');
  const sellingPriceValue = watch('sellingPrice');
  const costPriceValue = watch('costPrice');
  const quantityValue = watch('quantity');
  const deliveryTypeValue = watch('deliveryType');
  const descriptionValue = watch('description');

  const { mutate: createDish, isLoading, isSuccess } = useCreateDish();

  useEffect(() => {
    if (isSuccess) {
      toast.success('Dish Created Successfully!');
      navigate(`/restaurants/${query.get('restaurant')}`);
    }
  }, [isSuccess]);

  const file = files[0]?.file;
  const _handleReaderLoaded = (readerEvt) => {
    let binaryString = readerEvt.target.result;
    setBase64(btoa(binaryString));
  };
  useEffect(() => {
    if (file) {
      const reader = new FileReader();
      reader.onload = _handleReaderLoaded;
      reader.readAsBinaryString(file);
    }
  }, [file]);

  return (
    <div className='xl:col-span-2'>
      <form
        onSubmit={handleSubmit(async () => {
          const finalObj = {
            resturant_id: query.get('restaurant'),
            image: file,
            name: productNameValue,
            category: productCategoryValue?.value,
            price: sellingPriceValue,
            cost_price: costPriceValue,
            quantity: quantityValue,
            delivery_type: deliveryTypeValue?.value,
            description: descriptionValue
          };
          const formDataValue = new FormData();
          Object.keys(finalObj).forEach((key) => {
            formDataValue.append(key, finalObj[key]);
          });
          await fetch(`${API_URL}/createMenuOfResutrant`, {
            method: 'Post',
            body: formDataValue
          })
            .then((response) => response.json())
            .then((data) => {
              console.log({ data });
              toast.success(data.message);
              navigate(`/restaurants`);
            })
            .catch((err) => {
              console.log(err.message);
            });
        })}
        className='space-y-6'
      >
        <DishUploader files={files} setFiles={setFiles} />

        <div className='rounded-lg border border-default-200 p-6'>
          <div className='grid gap-6 lg:grid-cols-2'>
            <div className='space-y-6'>
              <TextFormInput
                name='productname'
                type='text'
                label='Product Name'
                placeholder='Product Name'
                control={control}
                fullWidth
              />

              <SelectFormInput
                name='productCatagory'
                label='Product Catagory'
                id='product-catagory'
                instanceId='product-catagory'
                control={control}
                options={[
                  { value: 'Italian', label: 'Italian' },
                  { value: 'BBQ', label: 'BBQ' },
                  { value: 'Mexican', label: 'Mexican' }
                ]}
                fullWidth
              />
              <div className='grid gap-6 lg:grid-cols-2'>
                <TextFormInput
                  name='sellingPrice'
                  type='text'
                  label='Selling Price'
                  placeholder='Selling Price'
                  control={control}
                  fullWidth
                />
                <TextFormInput
                  name='costPrice'
                  type='text'
                  label='Cost Price'
                  placeholder='Cost Price'
                  control={control}
                  fullWidth
                />
              </div>
              <TextFormInput
                name='quantity'
                type='text'
                label='Quantity'
                placeholder='Quantity in Stock'
                control={control}
                fullWidth
              />
              <SelectFormInput
                name='deliveryType'
                label='Delivery Type'
                id='delivery-catagory'
                instanceId='delivery-catagory'
                control={control}
                options={[
                  { value: 'Delivery', label: 'Delivery' },
                  { value: 'Pickup', label: 'Pickup' },
                  { value: 'Dine-in', label: 'Dine-in' }
                ]}
                fullWidth
              />
            </div>
            <div className='space-y-6'>
              <TextAreaFormInput
                name='description'
                label='Description'
                placeholder='short Description'
                rows={5}
                control={control}
                fullWidth
              />
              <div>
                <label
                  className='mb-2 block text-sm font-medium text-default-900'
                  htmlFor='editor'
                >
                  Product Long Description
                </label>
                <div id='editor' className='h-44'>
                  <ReactQuill
                    defaultValue={valueSnow}
                    theme='snow'
                    style={{ height: '180px', paddingBottom: '26px' }}
                    className='pb-1'
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className=''>
          <div className='flex flex-wrap items-center justify-end gap-4'>
            <div className='flex flex-wrap items-center gap-4'>
              <button
                type='reset'
                onClick={() => reset()}
                className='flex items-center justify-center gap-2 rounded-lg bg-red-500/10 px-6 py-2.5 text-center text-sm font-semibold text-red-500 shadow-sm transition-colors duration-200 hover:bg-red-500 hover:text-white'
              >
                <LuEraser size={20} />
                Clear
              </button>
              <button
                type='submit'
                className='flex items-center justify-center gap-2 rounded-lg bg-primary px-6 py-2.5 text-center text-sm font-semibold text-white shadow-sm transition-all duration-200 hover:bg-primary-500'
              >
                <LuSave size={20} />
                Save
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddDishForm;
