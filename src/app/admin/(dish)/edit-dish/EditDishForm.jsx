import {
  SelectFormInput,
  TextAreaFormInput,
  TextFormInput
} from '@/components';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { LuSave, LuUndo } from 'react-icons/lu';
import * as yup from 'yup';
//style
import { useEffect, useState } from 'react';
import { FaSpinner } from 'react-icons/fa6';
import 'react-quill/dist/quill.snow.css';
import { useNavigate, useParams } from 'react-router-dom';
import BusinessUploader from '../../(restaurant)/add-restaurant/BusinessUploader';
import { useUpdateDish } from '../../../../helpers/api';

const EditDishForm = ({ menuList, defaultValue }) => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [files, setFiles] = useState([
    // `data:image/png;base64,${menuList?.image}`,
  ]);
  const [base64, setBase64] = useState('');

  const { mutate: updateDish, isPending: updateLoading } = useUpdateDish(
    navigate,
    id
  );

  let valueSnow = '';
  valueSnow = `<h5><span class="ql-size-large">Mexican burritos are usually made with a wheat tortilla and contain grilled meat, cheese toppings</span></h5>`;

  const editDishFormSchema = yup.object({
    productname: yup.string().required('Please enter your product name'),
    // productCategory: yup
    //   .string()
    //   .required('Please select your product catagory'),
    productCategory: yup.object({
      label: yup.string().required(),
      value: yup.string().required()
    }),
    sellingPrice: yup.number().required('Please enter your selling price'),
    costPrice: yup.number().required('Please enter your selling price'),
    quantity: yup.number().required('Please enter your quantity'),
    // deliveryType: yup.string().required('Please select deliveryType'),
    deliveryType: yup.object({
      label: yup.string().required(),
      value: yup.string().required()
    }),

    description: yup.string().required('Please enter your description')
  });

  const { control, handleSubmit, reset, watch } = useForm({
    resolver: yupResolver(editDishFormSchema),
    defaultValues: defaultValue
  });

  const undoChanges = () => {
    reset(defaultValue);
  };

  const file = files[0]?.file;
  const _handleReaderLoaded = (readerEvt) => {
    let binaryString = readerEvt.target.result;
    console.log('before');

    setBase64(btoa(binaryString));
    console.log('after');
  };
  useEffect(() => {
    if (files.length > 0) {
      const file = files[0];
      if (file instanceof Blob) {
        const reader = new FileReader();
        reader.onload = _handleReaderLoaded;
        reader.readAsBinaryString(file);
      } else {
        console.error('The selected file is not a valid Blob:', file);
      }
    }
  }, [files]);
  console.log({ control });
  return (
    <div className='xl:col-span-2'>
      <form
        onSubmit={handleSubmit((values) => {
          console.log(values);
          updateDish(values);
        })}
        className='space-y-6'
      >
        {/* <EditDishUploader files={files} setFiles={setFiles} /> */}
        <BusinessUploader files={files} setFiles={setFiles} />
        <div className='rounded-lg border border-default-200 p-6'>
          <div className='grid gap-6 lg:grid-cols-2'>
            <div className='space-y-6'>
              <TextFormInput
                name='productname'
                type='text'
                label='Product Name'
                control={control}
                fullWidth
              />

              <SelectFormInput
                name='productCategory'
                label='Product Category'
                id='product-category'
                instanceId='product-category'
                control={control}
                options={[
                  { value: 'Italian', label: 'Italian' },
                  { value: 'BBQ', label: 'BBQ' },
                  { value: 'Mexican', label: 'Mexican' }
                ]}
                fullWidth
              />
              <div className='grid gap-6 lg:grid-cols-2'>
                <TextFormInput
                  name='sellingPrice'
                  type='text'
                  label='Selling Price'
                  control={control}
                  fullWidth
                />
                <TextFormInput
                  name='costPrice'
                  type='text'
                  label='Cost Price'
                  control={control}
                  fullWidth
                />
              </div>
              <TextFormInput
                name='quantity'
                type='text'
                label='Quantity'
                control={control}
                fullWidth
              />
              <SelectFormInput
                name='deliveryType'
                label='Delivery Type'
                id='delivery-category'
                instanceId='delivery-category'
                control={control}
                options={[
                  { value: 'Delivery', label: 'Delivery' },
                  { value: 'Pickup', label: 'Pickup' },
                  { value: 'Dine-in', label: 'Dine-in' }
                ]}
                fullWidth
              />
            </div>
            <div className='space-y-6'>
              <TextAreaFormInput
                name='description'
                label='Description'
                rows={5}
                control={control}
                fullWidth
              />
            </div>
          </div>
        </div>
        <div className=''>
          <div className='flex flex-wrap items-center justify-end gap-4'>
            <div className='flex flex-wrap items-center gap-4'>
              <button
                type='reset'
                onClick={undoChanges}
                className='inline-flex items-center gap-1 rounded-lg border border-primary bg-transparent px-5 py-2 text-center align-middle text-base font-semibold tracking-wide text-primary duration-500 hover:bg-primary hover:text-white'
              >
                <LuUndo size={20} />
                Cancel
              </button>
              <button
                disabled={updateLoading}
                type='submit'
                className='flex items-center justify-center gap-2 rounded-lg bg-primary px-6 py-2.5 text-center text-sm font-semibold text-white shadow-sm transition-all duration-200 hover:bg-primary-500'
              >
                {updateLoading ? <FaSpinner /> : <LuSave size={20} />}
                Save
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditDishForm;
