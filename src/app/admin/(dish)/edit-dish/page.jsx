import { BreadcrumbAdmin } from '@/components';
import { useMemo } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { useGetMenuOfResturaunt } from '../../../../helpers/api';
import EditDishForm from './EditDishForm';

const EditProduct = () => {
  const { id } = useParams();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const restaurantId = queryParams.get('restaurant');

  const { data: menuList, isLoading } = useGetMenuOfResturaunt(restaurantId);
  const dishData = menuList?.find((dish) => dish._id === id);
  const defaultValueX = useMemo(
    () => ({
      productname: dishData?.name,
      sellingPrice: dishData?.price,
      costPrice: dishData?.cost_price,
      quantity: dishData?.quantity,
      description: dishData?.description,
      deliveryType: dishData?.delivery_type,
      productCategory: dishData?.category
    }),
    [queryParams, restaurantId, menuList, dishData]
  );
  console.log(restaurantId, { defaultValueX }, { menuList }, { dishData });
  return (
    <>
      {!isLoading && menuList.length > 0 ? (
        <>
          <div className='w-full lg:ps-64'>
            <div className='page-content space-y-6 p-6'>
              <BreadcrumbAdmin
                title='Edit Dish'
                subtitle='Dishes'
                link='/admin/dishes'
              />
              <div className='grid gap-6 xl:grid-cols-3'>
                <EditDishForm
                  menuList={menuList}
                  defaultValue={defaultValueX}
                />
              </div>
            </div>
          </div>
        </>
      ) : (
        '...Loading'
      )}
    </>
  );
};

export default EditProduct;
