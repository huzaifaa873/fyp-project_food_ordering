import {
  BreadcrumbAdmin,
  DishDetailsSwiper,
  ProductDetailView
} from '@/components';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useGetMenuOfResturaunt } from '../../../../../helpers/api';

const DishDetails = () => {
  const { restaurantId } = useParams();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const restaurant_Id = queryParams.get('restaurant');
  console.log({ restaurantId });
  const navigate = useNavigate();
  const { data: menuOfRest, isLoading: isLoadingMenuOfRest } =
    useGetMenuOfResturaunt(restaurant_Id);
  const [dish, setDish] = useState();

  console.log({ menuOfRest });
  useEffect(() => {
    (async () => {
      if (menuOfRest?.length > 0) {
        const foundDish = menuOfRest?.find((menu) => menu._id === restaurantId);
        if (!foundDish) {
          // navigate('/login');
        } else {
          setDish(foundDish);
        }
      }
    })();
  }, [restaurantId, menuOfRest]);

  return (
    dish && (
      <div className='w-full lg:ps-64'>
        <div className='page-content space-y-6 p-6'>
          <BreadcrumbAdmin
            title={dish.name}
            subtitle='Dishes'
            link='/admin/dishes'
          />

          <div className='grid gap-6 lg:grid-cols-2'>
            <div className='rounded-lg border border-default-200 p-6'>
              <DishDetailsSwiper images={dish.image} />
            </div>
            <div className='rounded-lg border border-default-200 p-6'>
              <ProductDetailView dish={dish} />
            </div>
          </div>
        </div>
      </div>
    )
  );
};

export default DishDetails;
