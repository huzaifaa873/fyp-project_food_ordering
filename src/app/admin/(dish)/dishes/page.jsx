import { BreadcrumbAdmin, DishDataTable } from "@/components";
import { useParams } from "react-router-dom";
import { useGetDishList } from "../../../../helpers/api";
import { useCallback, useEffect, useMemo, useState } from "react";

const columns = [
  {
    key: "name",
    name: "Dish Name",
  },
  {
    key: "category_id",
    name: "Category",
  },
  {
    key: "price",
    name: "Price",
  },
  {
    key: "quantity",
    name: "Quantity",
  },
  {
    key: "create_by",
    name: "Created By",
  },
  {
    key: "status",
    name: "Status",
  },
];

const ProductList = () => {
  const { dishId } = useParams();
  console.log(dishId);
  const { data: dishList, isLoading: isLoadingDishList } =
    useGetDishList(dishId);
  console.log(dishList);

  const dummyApiDishList = [
    {
      _id: "6650fa61a68954730cc5d440",
      resturant_id: "6650f79b49113e5ecac3e73e",
      image: "asdfsdf",
      name: "Dish 1",
      category: "asdfsdf",
      price: 123123,
      cost_price: 123323,
      quantity: 1232,
      delivery_type: "asdfdsf",
      description: "sdfsdf",
      __v: 0,
    },
    {
      _id: "6650fa962e2efff67638aeef",
      resturant_id: "6650f79b49113e5ecac3e73e",
      image: "asdfsdf",
      name: "Dish 2",
      category: "asdfsdf",
      price: 123123,
      cost_price: 123323,
      quantity: 1232,
      delivery_type: "asdfdsf",
      description: "sdfsdf",
      __v: 0,
    },
  ];

  console.log(dishList);

  const dummyColumns = [
    { key: "name", name: "Dish Name" },
    { key: "category_id", name: "Category" },
    { key: "price", name: "Price" },
    { key: "quantity", name: "Quantity" },
    { key: "create_by", name: "Created By" },
    { key: "status", name: "Status" },
  ];

  // const dummyRows = [
  //   {
  //     id: 1001,
  //     category_id: 4,
  //     restaurant_id: 901,
  //     name: "Italian Pizza",
  //     images: ["pizzaImg"],
  //     price: 79,
  //     type: "non-veg",
  //     tags: ["Pizza", "Hot & Spicy", "Meal", "Bread"],
  //     quantity: 16,
  //     review: { count: 231, stars: 4.5 },
  //     sale: { discount: 50, type: "percent" },
  //     is_popular: true,
  //     category: {
  //       id: 4,
  //       name: "Pizza",
  //       image: "pizzaIconCategoryImg",
  //     },
  //     dish_id: 1001,
  //     create_by: "Admin",
  //     status: "published",
  //   },
  //   {
  //     id: 1001,
  //     category_id: 4,
  //     restaurant_id: 901,
  //     name: "Italian Pizza 2",
  //     images: ["pizzaImg"],
  //     price: 79,
  //     type: "non-veg",
  //     tags: ["Pizza", "Hot & Spicy", "Meal", "Bread"],
  //     quantity: 16,
  //     review: { count: 231, stars: 4.5 },
  //     sale: { discount: 50, type: "percent" },
  //     is_popular: true,
  //     category: {
  //       id: 4,
  //       name: "Pizza",
  //       image: "pizzaIconCategoryImg",
  //     },
  //     dish_id: 1001,
  //     create_by: "Admin",
  //     status: "published",
  //   },
  // ];

  const transformDishList = (dishList) => {
    return dishList.map((dish) => ({
      id: parseInt(dish._id, 16), // Converts the _id from hexadecimal string to a number
      category_id: 4, // Placeholder value, replace as needed
      restaurant_id: parseInt(dish.resturant_id, 16), // Converts the resturant_id from hexadecimal string to a number
      name: dish.name,
      images: [dish.image], // Converts single image to an array
      price: dish.price / 1000, // Assuming conversion for example, adjust as needed
      type: "veg", // Placeholder value, replace as needed
      tags: ["Example Tag"], // Placeholder value, replace as needed
      quantity: dish.quantity,
      review: { count: 0, stars: 0 }, // Placeholder value, replace as needed
      sale: { discount: 0, type: "none" }, // Placeholder value, replace as needed
      is_popular: false, // Placeholder value, replace as needed
      category: {
        id: 4, // Placeholder value, replace as needed
        name: dish.category,
        image: "exampleCategoryImage", // Placeholder value, replace as needed
      },
      dish_id: parseInt(dish._id, 16), // Converts the _id from hexadecimal string to a number
      create_by: "Admin", // Placeholder value, replace as needed
      status: "published", // Placeholder value, replace as needed
    }));
  };
  const rowsToInject = useMemo(() => {
    return transformDishList(dummyApiDishList);
  }, [dummyApiDishList]);

  const [renderCount, setRenderCount] = useState(0);
  useEffect(() => {
    // Increment renderCount state by 1
    setRenderCount((prevCount) => prevCount + 1);
  }, []); // Empty dependency array to ensure this effect only runs once after the initial render

  // Increment renderCount on every subsequent render
  useEffect(() => {
    if (renderCount > 0) {
      console.log(`Component has rendered ${renderCount} times`);
    }
  });
  console.log("render Count: " + renderCount);
  return (
    <div className="w-full lg:ps-64">
      <div className="page-content space-y-6 p-6">
        <BreadcrumbAdmin title="Dishes List" subtitle="Dishes" />

        <div className="grid grid-cols-1">
          <div className="rounded-lg border border-default-200">
            <DishDataTable
              rows={rowsToInject}
              columns={dummyColumns}
              title="Dishes List"
              buttonLink="/admin/add-dish"
              buttonText="Add Dish"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductList;
