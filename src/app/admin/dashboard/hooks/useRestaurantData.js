import { useSelector } from 'react-redux';

const useRestaurantData = () => {
  const { restaurantDashboard } = useSelector((state) => state.restaurant);
  const analyticsOverviewData = [
    {
      name: 'Total Revenue',
      amount: restaurantDashboard?.reviews || 0,
      change: '10% Increase'
    },
    // {
    //   name: "New Orders",
    //   amount: 2560,
    //   change: "50% Increase",
    // },
    {
      name: 'Received Orders',
      amount: restaurantDashboard?.reviews || 0,
      change: '34% Increase'
    },
    {
      name: 'Reviews',
      amount: restaurantDashboard?.reviews || 0,
      change: '5% Decrease'
    }
    // {
    //   name: 'New Reach',
    //   amount: 865,
    //   change: '48% Increase'
    // },
    // {
    //   name: 'Successful Orders',
    //   amount: 9165,
    //   change: '8% Decrease'
    // }
  ];
  return { analyticsOverviewData };
};

export default useRestaurantData;
