import { TextAreaFormInput, TextFormInput } from '@/components';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { LuSave, LuUndo } from 'react-icons/lu';
import { toast } from 'sonner';
import * as yup from 'yup';
import BusinessUploader from '../add-restaurant/BusinessUploader';
import { useEditObj } from './editContext';

const BusinessDetailForm = ({ resturant }) => {
  console.log(resturant);
  const [files, setFiles] = useState([`${resturant?.image}`]);

  const [base64, setBase64] = useState('');
  const { editObj, setEditObj } = useEditObj();
  const businessDetailsFormSchema = yup.object({
    businessName: yup.string().required('Please enter your business name'),
    businessType: yup.string().required('Please enter your business type'),
    contactNO: yup.number().required('Please enter your contact Number'),
    gstNo: yup.string().required('Please enter your GST NO.'),
    website: yup.string().required('Please enter website url'),
    email: yup
      .string()
      .email('Please enter a valid email')
      .required('Please enter your email'),
    description: yup.string().required('Please Enter your description')
  });

  console.log(resturant, { files });
  const defaultValue = {
    businessName: resturant?.businessName || '',
    businessType: resturant?.businessType || '',
    contactNO: resturant?.contact_no || '',
    gstNo: resturant?.gstNumber || '',
    website: resturant?.website || '',
    email: resturant?.businessEmail || '',
    description: resturant?.businessDescription || ''
  };

  const { control, handleSubmit, reset, watch } = useForm({
    resolver: yupResolver(businessDetailsFormSchema),
    defaultValues: defaultValue
  });

  const undoChanges = () => {
    reset(defaultValue);
  };
  const businessNameValue = watch('businessName');
  const businessTypeValue = watch('businessType');
  const contactNOValue = watch('contactNO'); // Note: Repeated field name, consider renaming one of them
  const gstNoValue = watch('gstNo');
  const websiteValue = watch('website');
  const emailValue = watch('email');
  const descriptionValue = watch('description');

  const file = files[0];
  const _handleReaderLoaded = (readerEvt) => {
    let binaryString = readerEvt.target.result;
    console.log('before');

    setBase64(btoa(binaryString));
    console.log('after');
  };
  useEffect(() => {
    if (file) {
      // const reader = new FileReader();
      // reader.onload = _handleReaderLoaded;
      // reader.readAsBinaryString(file);
    }
  }, [file]);
  console.log({ upload: file, base64 });

  return (
    <form
      onSubmit={handleSubmit(() => {
        setEditObj({
          ...editObj,
          image: file,
          businessName: businessNameValue,
          businessType: businessTypeValue,
          contact_no: contactNOValue,
          gstNumber: gstNoValue,
          website: websiteValue,
          businessEmail: emailValue,
          businessDescription: descriptionValue
        });
        toast.success('Saved!');
      })}
      id='tabBusinessDetail'
      className='hidden'
      role='tabpanel'
    >
      <h4 className='mb-6 text-lg font-medium text-default-900'>Step 2:</h4>
      <div className='mb-6 grid gap-6 lg:grid-cols-2'>
        <TextFormInput
          name='businessName'
          type='text'
          label='Business Name'
          control={control}
          fullWidth
        />
        <TextFormInput
          name='businessType'
          type='text'
          label='Business Type'
          control={control}
          fullWidth
        />
        <TextFormInput
          name='contactNO'
          type='text'
          label='Contact Number'
          control={control}
          fullWidth
        />
        <TextFormInput
          name='gstNo'
          type='text'
          label='GST Number'
          control={control}
          fullWidth
        />
        <TextFormInput
          name='website'
          type='text'
          label='Website'
          control={control}
          fullWidth
        />
        <TextFormInput
          name='email'
          type='email'
          label='Email'
          control={control}
          fullWidth
        />
        <BusinessUploader files={files} setFiles={setFiles} />
        <img alt='Img' src={`${resturant?.image}`}></img>
        <TextAreaFormInput
          name='description'
          label='Description'
          placeholder='Enter Description'
          rows={5}
          containerClassName='lg:col-span-2'
          control={control}
          fullWidth
        />
      </div>
      <div className='flex flex-wrap justify-end gap-4'>
        <button
          type='reset'
          onClick={undoChanges}
          className='inline-flex items-center gap-1 rounded-lg border border-primary bg-transparent px-5 py-2 text-center align-middle text-base font-semibold tracking-wide text-primary duration-500 hover:bg-primary hover:text-white'
        >
          <LuUndo size={20} />
          Cancel
        </button>
        <button
          type='submit'
          className='flex items-center justify-center gap-2 rounded-lg bg-primary px-6 py-2.5 text-center text-sm font-semibold text-white shadow-sm transition-all duration-200 hover:bg-primary-500'
        >
          <LuSave size={20} />
          Save
        </button>
      </div>
    </form>
  );
};

export default BusinessDetailForm;
