import { createContext, useContext, useState } from "react";

const EditContext = createContext();
export const EditProvider = ({ children }) => {
  const [editObj, setEditObj] = useState({ abc: 1 });
  console.log("Context editObj", editObj);
  return (
    <EditContext.Provider value={{ editObj, setEditObj }}>
      {children}
    </EditContext.Provider>
  );
};
export const useEditObj = () => {
  return useContext(EditContext);
};
