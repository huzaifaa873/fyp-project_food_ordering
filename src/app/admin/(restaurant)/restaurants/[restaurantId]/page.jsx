import {
  avatar1Img,
  avatar2Img,
  restaurant1Img,
  restaurantBgImg
} from '@/assets/data/images';
import { BreadcrumbAdmin } from '@/components';
import { FaStar } from 'react-icons/fa6';
import { useNavigate, useParams } from 'react-router-dom';
import { dishesData } from '../../../../../assets/data/dishes';
import { RestaurantMenuDataTable } from '../../../../../components';
import {
  useGetMenuOfResturaunt,
  useGetResturant
} from '../../../../../helpers/api';

const columns = [
  {
    key: 'name',
    name: 'Dish Name'
  },
  {
    key: 'delivery_type',
    name: 'Delivery Type'
  },
  {
    key: 'price',
    name: 'Price'
  },
  {
    key: 'productId',
    name: 'Prodcut ID'
  },
  {
    key: 'quantity',
    name: 'Quantity'
  }
];

const RestaurantDetails = () => {
  const navigate = useNavigate();
  const { restaurantId } = useParams();
  console.log(restaurantId);
  // const { data: resturantList, isLoading } = useGetResturantList();
  const { data: menuOfRest, isLoading: isLoadingMenuOfRest } =
    useGetMenuOfResturaunt(restaurantId);

  const { data: getRestaurant, isLoading: getRestaurantLoading } =
    useGetResturant(restaurantId);
  console.log({ restaurantId, getRestaurant, menuOfRest });

  const restaurant = getRestaurant;
  console.log(dishesData);
  const dummyRows = [
    {
      id: 1001,
      category_id: 4,
      restaurant_id: 901,
      name: 'Italian Pizza',
      images: ['/yum_r/src/assets/images/dishes/pizza.png']
    }
  ];

  console.log('menuOfRest', menuOfRest);
  const transformData = (data) => {
    return menuOfRest?.map((item) => ({
      id: item?._id,
      productId: item?._id,
      price: item?.price,
      name: item?.name,
      images: item?.image,
      delivery_type: item?.delivery_type,
      quantity: item?.quantity
    }));
  };
  const rowsToInject = transformData(menuOfRest);
  console.log(rowsToInject);

  return (
    <div className='w-full lg:ps-64'>
      <div className='page-content space-y-6 p-6'>
        <BreadcrumbAdmin
          title={restaurant?.businessName ?? 'Restaurant Detail'}
          subtitle='Restaurants'
          link='/restaurants'
        />
        <div className='mb-6 rounded-lg border border-default-200 p-6'>
          <img
            src={restaurantBgImg}
            className='hidden w-full md:flex'
            alt='background'
          />
          <div className='flex items-center gap-3 md:-mt-14 md:items-end'>
            <img
              src={restaurant1Img}
              className='h-28 w-28 rounded-full bg-default-50'
              alt='restaurant'
            />
            <div>
              <h4 className='mb-1 text-base font-medium text-default-800'>
                {restaurant?.businessName ?? 'Restaurant Detail'}
              </h4>
              <p className='text-sm text-default-600'>
                {restaurant?.businessType}
              </p>
            </div>
          </div>
        </div>
        <div className='grid grid-cols-1 gap-6 xl:grid-cols-3'>
          <div className='xl:col-span-2'>
            <RestaurantMenuDataTable
              columns={columns}
              rows={rowsToInject?.length > 0 ? rowsToInject : []}
              buttonLink={`/super-admin/add-dish?restaurant=${restaurantId}`}
              buttonText='Add Dish'
              title='Menu'
              restaurantId={restaurantId}
            />
          </div>
          <div className='xl:col-span-1'>
            <div className='mb-6 rounded-lg border border-default-200'>
              <div className='border-b border-b-default-300 p-6'>
                <h4 className='text-xl font-medium text-default-900'>
                  Seller Personal Detail
                </h4>
              </div>
              <div className='px-6 py-5'>
                <table cellPadding={10}>
                  <tbody>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Owner Name:
                      </td>
                      <td className='text-start'>
                        {restaurant?.first_name} {restaurant?.last_name}
                      </td>
                    </tr>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Resturant ID:
                      </td>
                      <td className='text-start'>{restaurant?._id || '--'}</td>
                    </tr>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Email:
                      </td>
                      <td className='text-start'>{restaurant?.email}</td>
                    </tr>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Contact No:
                      </td>
                      <td className='text-start'>
                        {restaurant?.contactNumber}
                      </td>
                    </tr>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Region:
                      </td>
                      <td className='text-start'>
                        {restaurant?.region || '--'}
                      </td>
                    </tr>
                    <tr>
                      <td className='text-start text-base font-medium'>
                        Location:
                      </td>
                      <td className='text-start'>
                        {restaurant?.country} {restaurant?.city}{' '}
                        {restaurant?.address}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className='mb-6 rounded-lg border border-default-200'>
              <div className='border-b border-b-default-300 p-6'>
                <h4 className='text-xl font-medium text-default-900'>
                  Customer Reviews
                </h4>
              </div>
              <div className='p-6'>
                <div className='mb-6'>
                  <div className='flex items-center gap-2'>
                    <h5 className='text-sm'>5</h5>
                    <div className='flex h-2 w-full overflow-hidden rounded-lg bg-default-100'>
                      <div className='w-full rounded-lg bg-yellow-400' />
                    </div>
                  </div>
                  <div className='flex items-center gap-2'>
                    <h5 className='text-sm'>4</h5>
                    <div className='flex h-2 w-full overflow-hidden rounded-lg bg-default-100'>
                      <div className='w-4/5 rounded-lg bg-yellow-400' />
                    </div>
                  </div>
                  <div className='flex items-center gap-2'>
                    <h5 className='text-sm'>3</h5>
                    <div className='flex h-2 w-full overflow-hidden rounded-lg bg-default-100'>
                      <div className='w-3/5 rounded-lg bg-yellow-400' />
                    </div>
                  </div>
                  <div className='flex items-center gap-2'>
                    <h5 className='text-sm'>2</h5>
                    <div className='flex h-2 w-full overflow-hidden rounded-lg bg-default-100'>
                      <div className='w-2/5 rounded-lg bg-yellow-400' />
                    </div>
                  </div>
                  <div className='flex items-center gap-2'>
                    <h5 className='text-sm'>1</h5>
                    <div className='flex h-2 w-full overflow-hidden rounded-lg bg-default-100'>
                      <div className='w-1/5 rounded-lg bg-yellow-400' />
                    </div>
                  </div>
                </div>
                <div className='mb-6 flex justify-around'>
                  <div className='text-center'>
                    <h2 className='mb-1 text-2xl font-medium text-default-900'>
                      4.5{' '}
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                    </h2>
                    <p className='block text-xs text-default-600'>
                      452 Reviews
                    </p>
                  </div>
                  <div className='text-center'>
                    <h2 className='mb-1 text-2xl font-medium text-default-900'>
                      91%
                    </h2>
                    <p className='block text-xs text-default-600'>
                      Recommended
                    </p>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='mb-4 flex items-center gap-3'>
                    <img
                      src={avatar1Img}
                      className='h-11 w-11 rounded-full'
                      alt='avatar'
                    />
                    <div className='flex-grow'>
                      <h4 className='mb-1 text-xs text-default-700'>
                        Kianna Stanton{' '}
                        <span className='text-default-600'>🇺🇸US</span>
                      </h4>
                      <h4 className='text-xs text-green-400'>Verified Buyer</h4>
                    </div>
                    <div>
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                    </div>
                  </div>
                  <h5 className='mb-2 text-sm text-default-600'>
                    SO DELICIOUS 🍯💯
                  </h5>
                  <p className='text-sm text-default-600'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                  </p>
                </div>
                <div className='mb-4'>
                  <div className='mb-4 flex items-center gap-3'>
                    <img
                      src={avatar2Img}
                      className='h-11 w-11 rounded-full'
                      alt='avatar'
                    />
                    <div className='flex-grow'>
                      <h4 className='mb-1 text-xs text-default-700'>
                        Ryan Rhiel Madsen{' '}
                        <span className='text-default-600'>🇺🇸US</span>
                      </h4>
                      <h4 className='text-xs text-green-400'>Verified Buyer</h4>
                    </div>
                    <div>
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                      <FaStar
                        size={20}
                        className='inline-flex fill-yellow-400 align-middle text-yellow-400'
                      />
                    </div>
                  </div>
                  <h5 className='mb-2 text-sm text-default-600'>
                    SO DELICIOUS 🍯💯
                  </h5>
                  <p className='text-sm text-default-600'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RestaurantDetails;
