import {
  BreadcrumbAdmin,
  ProductPagination,
  RestaurantListCard,
} from "@/components";
import { restaurantsData } from "@/assets/data";
import { useGetResturantList } from "../../../../helpers/api";

const RestaurantList = () => {
  const { data: resturantList, isLoading } = useGetResturantList();
  console.log("resturantList", resturantList);

  return (
    <div className="w-full lg:ps-64">
      <div className="page-content space-y-6 p-6">
        <BreadcrumbAdmin title="Restaurants List" subtitle="Restaurants" />
        <div className="mb-6 grid gap-6 md:grid-cols-2 2xl:grid-cols-4">
          {/* {restaurantsData.map((restaurant) => (
            <RestaurantListCard key={restaurant.id} restaurant={restaurant} />
          ))} */}
          {!isLoading &&
            resturantList?.map((restaurant) => (
              <RestaurantListCard
                key={restaurant?._id}
                restaurant={restaurant}
              />
            ))}
        </div>
        <ProductPagination />
      </div>
    </div>
  );
};

export default RestaurantList;
