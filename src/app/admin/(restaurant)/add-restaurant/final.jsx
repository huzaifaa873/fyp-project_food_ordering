import { createContext, useContext, useState } from "react";

const FinalContext = createContext();
export const FinalProvider = ({ children }) => {
  const [finalObj, setFinalObj] = useState({});
  console.log("Context finalObj", finalObj);
  return (
    <FinalContext.Provider value={{ finalObj, setFinalObj }}>
      {children}
    </FinalContext.Provider>
  );
};
export const useFinalObj = () => {
  return useContext(FinalContext);
};
