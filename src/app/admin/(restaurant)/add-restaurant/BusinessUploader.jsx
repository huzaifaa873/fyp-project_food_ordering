import FilePondPluginImageCrop from 'filepond-plugin-image-crop';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import { FilePond, registerPlugin } from 'react-filepond';

import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import 'filepond/dist/filepond.min.css';

// Register the plugins
registerPlugin(
  FilePondPluginImageExifOrientation,
  FilePondPluginImagePreview,
  FilePondPluginImageCrop
);

const BusinessUploader = ({ files, setFiles }) => {
  let pond = null;
  const onSubmit = () => {
    const formData = new FormData();
    // files
    //   .map((item) => item.file)
    //   .forEach((file) => formData.append("my-file", file));
    // console.log(formData);
    console.log('pond', pond);

    if (pond) {
      const files = pond.getFiles();
      files.forEach((file) => {
        console.log('each file', file, file.getFileEncodeBase64String());
      });

      pond
        .processFiles(files)
        .then((res) => console.log(res))
        .catch((error) => console.log('err', error));
    }
  };

  console.log('files', files[0]);
  return (
    <div className='rounded-lg border border-default-200 p-6 '>
      <div className='mb-4 flex h-96 flex-col items-center justify-center rounded-lg border border-default-200 p-6 '>
        <FilePond
          files={files}
          ref={(ref) => {
            pond = ref;
          }}
          // required
          acceptedFileTypes={['image/*']}
          // fileValidateTypeDetectType={(source, type) =>
          //   // Note: we need this here to activate the file type validations and filtering
          //   new Promise((resolve, reject) => {
          //     // Do custom type detection here and return with promise
          //     resolve(type);
          //   })
          // }
          onupdatefiles={setFiles}
          allowMultiple={false}
          className='h-28 w-28 md:h-56 md:w-56 lg:h-64 lg:w-64'
          labelIdle='<div class="lg:mt-44 md:mt-36 mt-9">Upload Image</div>'
          imagePreviewHeight={250}
          imageCropAspectRatio='1:1'
          styleButtonRemoveItemPosition='center bottom'
        />
      </div>
    </div>
  );
};

export default BusinessUploader;
