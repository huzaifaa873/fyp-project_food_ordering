import {
  PasswordFormInput,
  SelectFormInput,
  TextFormInput
} from '@/components';
import { LuCopy, LuLock, LuMail, LuShield } from 'react-icons/lu';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import useLogin from './useLogin';

const LoginForm = () => {
  const { login, control, changeUserRole } = useLogin();
  const { loading } = useSelector((state) => state.authUser);

  return (
    <>
      <form onSubmit={login}>
        <SelectFormInput
          name='role'
          label='Role'
          control={control}
          id='role'
          instanceId='role-id'
          options={[
            { value: 'superAdmin', label: 'Super Admin' },
            { value: 'admin', label: 'Admin' },
            { value: 'user', label: 'Customer' }
          ]}
        />
        <TextFormInput
          name='email'
          control={control}
          type='email'
          className='bg-white '
          placeholder='Enter your email'
          label='Email'
          containerClassName='mb-6 mt-2'
          fullWidth
        />

        <PasswordFormInput
          name='password'
          control={control}
          label='Password'
          labelClassName='block text-sm font-medium text-default-900 mb-2'
          containerClassName='mb-1'
          fullWidth
        />

        <Link
          to='/auth/forgot-password'
          className='float-right text-end text-sm text-default-600 underline'
        >
          Forgot Password?
        </Link>

        <button
          type='submit'
          className='mt-5 w-full rounded-lg bg-primary px-6 py-3 text-base capitalize text-white transition-all hover:bg-primary-500'
          disabled={loading}
        >
          Log In
        </button>
      </form>

      <div className='mt-6 flex flex-col justify-center gap-4'>
        <div className='flex flex-col gap-2 rounded-lg border border-dashed border-primary'>
          <div className='flex items-center justify-between gap-2 border-b border-dashed border-primary px-4 py-2'>
            <div className='inline-flex items-center gap-2'>
              <LuShield size={22} />
              <p className='text-base text-default-900'>Administator</p>
            </div>
            <button
              className='flex items-center gap-1.5 rounded-md bg-primary px-2 py-1 text-sm text-white transition-all hover:bg-primary-600'
              onClick={() => changeUserRole('admin')}
            >
              <LuCopy />
              Use
            </button>
          </div>
          <p className='p-2 px-4'>
            <span className='flex items-center gap-2 text-sm'>
              <LuMail size={18} /> admin@coderthemes.com
            </span>
            <span className='mt-2 flex items-center gap-2 text-sm'>
              <LuLock size={18} /> password
            </span>
          </p>
        </div>

        <div className='flex flex-col gap-2 rounded-lg border border-dashed border-primary'>
          <div className='flex items-center justify-between gap-2 border-b border-dashed border-primary px-4 py-2'>
            <div className='inline-flex items-center gap-2'>
              <LuShield size={22} />
              <p className='text-base text-default-900'>Super Admin</p>
            </div>
            <button
              className='flex items-center gap-1.5 rounded-md bg-primary px-2 py-1 text-sm text-white transition-all hover:bg-primary-600'
              onClick={() => changeUserRole('superAdmin')}
            >
              <LuCopy />
              Use
            </button>
          </div>
          <p className='p-2 px-4'>
            <span className='flex items-center gap-2 text-sm'>
              <LuMail size={18} /> superAdmin@coderthemes.com
            </span>
            <span className='mt-2 flex items-center gap-2 text-sm'>
              <LuLock size={18} /> password
            </span>
          </p>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
