import { useAuthContext } from '@/context';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { toast } from 'sonner';
import * as yup from 'yup';
import { userLoginAction } from '../../../redux/auth/authActions';

const useLogin = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { session, saveSession } = useAuthContext();
  const [searchParams] = useSearchParams();

  const loginFormSchema = yup.object({
    role: yup.object({
      value: yup.string().required(),
      label: yup.string().required()
    }),

    email: yup
      .string()
      .email('Please enter a valid email')
      .required('Please enter your email'),
    password: yup.string().required('Please enter your password')
  });

  const { control, handleSubmit, reset } = useForm({
    resolver: yupResolver(loginFormSchema),
    defaultValues: {
      role: {
        value: 'superAdmin',
        label: 'Super Admin'
      },
      email: 'shahidmuneerawan@gmail.com',
      password: '1234'
    }
  });

  const changeUserRole = (role) => {
    reset({
      role: {
        value: role,
        label: role
      },
      email:
        role == 'user'
          ? 'harry@gmail.com'
          : role == 'admin'
            ? 'shahidmuneerawan@gmail.com'
            : 'shahidmuneerawan@gmail.com',
      password: '1234'
    });
  };

  useEffect(() => {
    if (session) {
      redirectAsRole(session.role);
    }
  }, []);

  const redirectAsRole = (role, current = false) => {
    const redirectLink = searchParams.get('redirectTo');
    const adminRoute = redirectLink?.startsWith('/admin');
    const superAdminRoute = redirectLink?.startsWith('/super-admin');

    console.log({ role, redirectLink, adminRoute });
    if (role == 'admin' && !(redirectLink && !adminRoute)) {
      navigate(redirectLink && adminRoute ? redirectLink : '/admin/dashboard');
      return;
    } else if (role == 'user' && !(redirectLink && adminRoute)) {
      navigate(redirectLink && !adminRoute ? redirectLink : '/home');
      return;
    } else if (role == 'superAdmin' && !(redirectLink && !superAdminRoute)) {
      navigate(redirectLink && !adminRoute ? redirectLink : '/restaurants');
      return;
    }

    if (current) {
      if (role == 'admin') {
        navigate('/admin/dashboard');
      } else if (role == 'superAdmin') {
        navigate('/super-admin');
      } else if (role == 'user') {
        navigate('/home');
      }
    }
  };

  const login = handleSubmit(async (values) => {
    try {
      console.log({ values });
      dispatch(
        userLoginAction({
          data: values,
          callback: (res) => {
            console.log({ res });
            if (res.token) {
              toast.success('Successfully logged in. Redirecting....', {
                position: 'top-right',
                duration: 2000
              });
              saveSession({
                ...(res ?? {}),
                token: res.token
              });
              if (
                res.role == 'user' ||
                res.role == 'admin' ||
                res.role == 'superAdmin'
              ) {
                redirectAsRole(res.role, true);
              }
            }
          }
        })
      );

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e) {
      if (e.response?.data?.error) {
        toast.error(e.response?.data?.error, {
          position: 'top-right',
          duration: 2000
        });
      }
    } finally {
    }
  });

  return { login, control, changeUserRole };
};

export default useLogin;
