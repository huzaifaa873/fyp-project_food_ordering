import { useAuthContext } from '@/context';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { toast } from 'sonner';
import * as yup from 'yup';
import { userRegisterAction } from '../../../redux/auth/authActions';

const useSignUp = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { session, saveSession } = useAuthContext();
  const [searchParams] = useSearchParams();

  const registerFormSchema = yup.object({
    firs_name: yup.string(),
    last_name: yup.string(),
    email: yup
      .string()
      .email('Please enter a valid email')
      .required('Please enter your email'),
    password: yup.string().required('Please enter your password')
  });

  const { control, handleSubmit } = useForm({
    resolver: yupResolver(registerFormSchema),
    defaultValues: {
      first_name: '',
      last_name: '',
      email: '',
      password: ''
    }
  });

  useEffect(() => {
    if (session) {
      redirectAsRole(session.role);
    }
  }, []);

  const redirectAsRole = (role, current = false) => {
    const redirectLink = searchParams.get('redirectTo');
    const adminRoute = redirectLink?.startsWith('/admin');
    const superAdminRoute = redirectLink?.startsWith('/super-admin');

    console.log({ role, redirectLink, adminRoute });
    if (role == 'admin' && !(redirectLink && !adminRoute)) {
      navigate(redirectLink && adminRoute ? redirectLink : '/admin/dashboard');
      return;
    } else if (role == 'user' && !(redirectLink && adminRoute)) {
      navigate(redirectLink && !adminRoute ? redirectLink : '/home');
      return;
    } else if (role == 'superAdmin' && !(redirectLink && !superAdminRoute)) {
      navigate(redirectLink && !adminRoute ? redirectLink : '/super-admin');
      return;
    }

    if (current) {
      if (role == 'admin') {
        navigate('/admin/dashboard');
      } else if (role == 'superAdmin') {
        navigate('/super-admin');
      } else if (role == 'user') {
        navigate('/home');
      }
    }
  };

  const signup = handleSubmit(async (values) => {
    try {
      console.log({ values });
      dispatch(
        userRegisterAction({
          data: values,
          callback: (res) => {
            console.log({ res });
            if (res.token) {
              toast.success('Successfully Sign up. Redirecting....', {
                position: 'top-right',
                duration: 2000
              });
              saveSession({
                ...(res ?? {}),
                token: res.token
              });
              if (
                res.role == 'user' ||
                res.role == 'admin' ||
                res.role == 'superAdmin'
              ) {
                redirectAsRole(res.role, true);
              }
            }
          }
        })
      );

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e) {
      if (e.response?.data?.error) {
        toast.error(e.response?.data?.error, {
          position: 'top-right',
          duration: 2000
        });
      }
    } finally {
    }
  });

  return { signup, control };
};

export default useSignUp;
