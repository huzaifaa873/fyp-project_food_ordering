import { PasswordFormInput, TextFormInput } from '@/components';
import useSignUp from './useSignUp';

const RegisterForm = () => {
  const { control, signup } = useSignUp();

  return (
    <form onSubmit={signup}>
      <div className='flex gap-2 w-full justify-between'>
        <TextFormInput
          name='first_name'
          control={control}
          type='text'
          className='bg-white'
          placeholder='Enter your first name'
          containerClassName='mb-6'
          label='First Name'
          fullWidth
        />
        <TextFormInput
          name='last_name'
          control={control}
          type='text'
          className='bg-white '
          placeholder='Enter your last name'
          containerClassName='mb-6'
          label='Last Name'
          fullWidth
        />
      </div>

      <TextFormInput
        name='email'
        control={control}
        type='email'
        placeholder='Enter your email'
        className='bg-white'
        containerClassName='mb-6'
        label='Email'
        fullWidth
      />

      <PasswordFormInput
        name='password'
        control={control}
        label='Password'
        containerClassName='mb-6'
        fullWidth
      />

      <button
        type='submit'
        className='w-full rounded-lg bg-primary px-6 py-3 text-base capitalize text-white transition-all hover:bg-primary-500'
      >
        Register
      </button>
    </form>
  );
};

export default RegisterForm;
