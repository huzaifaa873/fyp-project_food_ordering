import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import { encryptTransform } from 'redux-persist-transform-encrypt';
import storage from 'redux-persist/lib/storage';
import authSlice from './auth/authSlice';
import restaurantDashboardSlice from './resturants/dashboard/dashboardSlice';
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['authUser'],
  transforms: [
    encryptTransform({
      secretKey: '2327682dwiue82369yew982999838'
    })
  ]
};
const combine_reducer = combineReducers({
  authUser: authSlice,
  restaurant: restaurantDashboardSlice
});

export const store = configureStore({
  reducer: persistReducer(persistConfig, combine_reducer),
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      serializableCheck: false
    }).concat();
  }
});

export const persistedStore = persistStore(store);
