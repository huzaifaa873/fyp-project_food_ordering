import { createSlice } from '@reduxjs/toolkit';
import {
  getRecentOrdersAction,
  getRestaurantCategoriesAction,
  getRestaurantDashboardDetailsAction
} from './dashboardActions';
const initialState = {
  loading: false,
  error: null,
  restaurantDashboard: null,
  categoryLoading: false,
  restaurantCategory: null,
  recentLoading: false,
  recentOrders: null
};
const restaurantDashboardSlice = createSlice({
  name: 'restaurant',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getRestaurantDashboardDetailsAction.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(
        getRestaurantDashboardDetailsAction.fulfilled,
        (state, action) => {
          state.loading = false;
          state.restaurantDashboard = action.payload;
        }
      )
      .addCase(
        getRestaurantDashboardDetailsAction.rejected,
        (state, action) => {
          state.loading = false;
          state.error = action.error.message;
        }
      )
      .addCase(getRestaurantCategoriesAction.pending, (state) => {
        state.categoryLoading = true;
        state.error = null;
      })
      .addCase(getRestaurantCategoriesAction.fulfilled, (state, action) => {
        state.categoryLoading = false;
        state.restaurantCategory = action.payload;
      })
      .addCase(getRestaurantCategoriesAction.rejected, (state, action) => {
        state.categoryLoading = false;
        state.error = action.error.message;
      })
      .addCase(getRecentOrdersAction.pending, (state) => {
        state.recentLoading = true;
        state.error = null;
      })
      .addCase(getRecentOrdersAction.fulfilled, (state, action) => {
        state.recentLoading = false;
        state.recentOrders = action.payload;
      })
      .addCase(getRecentOrdersAction.rejected, (state, action) => {
        state.recentLoading = false;
        state.error = action.error.message;
      });
  }
});

export default restaurantDashboardSlice.reducer;
