import { createAsyncThunk } from '@reduxjs/toolkit';
import { get } from '../../../helpers/httpClient';

export const getRestaurantDashboardDetailsAction = createAsyncThunk(
  'restaurant/getRestaurantDashboardDetails',
  async ({ id }, { dispatch, rejectWithValue }) => {
    try {
      const paramBody = {
        resturant_id: id
      };
      const response = await get('/getDashboardfResutrant', {
        params: paramBody
      });
      return response;
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.response.data);
    }
  }
);

export const getRestaurantCategoriesAction = createAsyncThunk(
  'restaurant/getRestaurantCategories',
  async ({ id }, { dispatch }) => {
    try {
      const paramBody = {
        resturant_id: id
      };
      const response = await get('/getCategoryOfResutrant', {
        params: paramBody
      });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
);

export const getRecentOrdersAction = createAsyncThunk(
  'restaurant/getRecentOrders',
  async ({ id }, { dispatch }) => {
    try {
      const paramBody = {
        resturant_id: id
      };
      const response = await get('/getRecentOrdersOfResutrant', {
        params: paramBody
      });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
);
