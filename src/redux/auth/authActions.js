import { createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'sonner';
import { post } from '../../helpers/httpClient';

export const userLoginAction = createAsyncThunk(
  'authUser/userLogin',
  async ({ data, callback }, { dispatch, rejectWithValue }) => {
    try {
      console.log({
        data
      });
      let endpoint =
        data.role.value === 'user'
          ? '/loginUser'
          : data.role.value === 'admin'
            ? '/loginAdmin'
            : '/loginAdmin';

      const res = await post(`${endpoint}`, data);
      callback(res);
      return res;
    } catch (e) {
      toast.error(e.response?.data?.message, {
        position: 'top-right',
        duration: 2000
      });
      return rejectWithValue(e.response.data);
    }
  }
);

export const userRegisterAction = createAsyncThunk(
  'authUser/userRegister',
  async ({ data, callback }, { dispatch, rejectWithValue }) => {
    try {
      const res = await post('/registeruser', data);
      callback(res);
      return res;
    } catch (e) {
      toast.error(e.response?.data?.message, {
        position: 'top-right',
        duration: 2000
      });
      return rejectWithValue(e.response.data);
    }
  }
);
