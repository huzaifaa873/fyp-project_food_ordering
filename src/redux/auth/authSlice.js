import { createSlice } from '@reduxjs/toolkit';
import { userLoginAction, userRegisterAction } from './authActions';
const initialState = {
  loading: false,
  error: null,
  authUser: null
};
const authSlice = createSlice({
  name: 'authUser',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(userLoginAction.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(userLoginAction.fulfilled, (state, action) => {
        state.loading = false;
        state.authUser = action.payload;
      })
      .addCase(userLoginAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(userRegisterAction.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(userRegisterAction.fulfilled, (state, action) => {
        state.loading = false;
        state.authUser = action.payload;
      })
      .addCase(userRegisterAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  }
});

export default authSlice.reducer;
