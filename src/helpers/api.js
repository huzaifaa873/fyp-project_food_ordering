import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { del, get, post, postFormData, putFormData } from './httpClient';
// import { toast } from "react-toastify";
import { toast } from 'sonner';

export const useGetResturantList = () => {
  return useQuery({
    queryKey: ['resturants'],
    queryFn: () => get('/getResturantlist')
  });
};

export const useGetResturant = (id) => {
  return useQuery({
    queryKey: ['resturants', id],
    queryFn: () => get(`/getoneResturant?id=${id}`),
    enabled: !!id
  });
};

export const useDelResturant = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: (id) => {
      return del(`/deleteRestaurant?id=${id}`);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    },
    onSuccess: (response) => {
      toast.success('Sucessfully deleted Resturant!');
      queryClient.invalidateQueries(['resturants', 'resturants']);
    }
  });
};

export const useDelResturantMenu = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: (id) => {
      return del(`/deleteMenuOfRestaurant?id=${id}`);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    },
    onSuccess: (response) => {
      toast.success('Sucessfully deleted Resturant Menu!');
      queryClient.invalidateQueries(['dishList', 'resturants']);
    }
  });
};

export const useCreateRestAdmin = (navigate) => {
  return useMutation({
    mutationFn: (values) => {
      return postFormData('/createResturantAdmin', values);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    },
    onSuccess: (response) => {
      navigate('/restaurants');
      toast.success('Successfully created Resturant!');
    }
  });
};

export const useUpdateRestAdmin = (navigate, id) => {
  return useMutation({
    mutationFn: (values) => {
      return putFormData(`/updateRestaurantAdmin?id=${id}`, values);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    },
    onSuccess: (response) => {
      navigate('/restaurants');
      toast.success('Successfully updated Resturant!');
    }
  });
};

//Menu
export const useGetMenuOfResturaunt = (restaurant_id) => {
  return useQuery({
    queryKey: ['dishList', restaurant_id],
    queryFn: () => get(`/getMenuOfResutrant?resturant_id=${restaurant_id}`),
    enabled: !!restaurant_id
  });
};

export const useCreateDish = () => {
  return useMutation({
    mutationFn: (values) => {
      return post('/createMenuOfResutrant', values);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    }
  });
};

export const useGetDishList = (dishId) => {
  const id = dishId?.trim()?.length > 0 ? dishId : `6650f79b49113e5ecac3e73e`;

  return useQuery({
    queryKey: ['dishList', dishId],
    queryFn: () => get(`/getMenuOfResutrant?resturant_id=${id}`),
    enabled: !!dishId
  });
};
export const useUpdateDish = (navigate, id) => {
  return useMutation({
    mutationFn: (values) => {
      return putFormData(`/updateMenuOfRestaurant?id=${id}`, values);
    },
    onError: (err) => {
      toast.error(err?.response?.data?.message || err.message);
    },
    onSuccess: (response) => {
      navigate('/restaurants');
      toast.success('Successfully updated Resturant!');
    }
  });
};
