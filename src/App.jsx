import { Toaster } from "sonner";
import { AppProvidersWrapper, BackToTop } from "./components";
import AllRoutes from "./routes/Routes";
import { configureFakeBackend } from "@/common";
import { ToastContainer } from "react-toastify";

// styles
import "@/assets/css/style.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { useState } from "react";
configureFakeBackend();

const queryClientOptions = {
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
};
const App = () => {
  const [client] = useState(new QueryClient(queryClientOptions));
  return (
    <AppProvidersWrapper>
      <QueryClientProvider client={client}>
        <AllRoutes />
        <BackToTop />
        <Toaster richColors />
      </QueryClientProvider>
      <ToastContainer
        position="bottom-right"
        theme="colored"
        autoClose={2000}
        hideProgressBar
        newestOnTop
        closeOnClick
        pauseOnFocusLoss
        pauseOnHover
        limit={5}
      />
    </AppProvidersWrapper>
  );
};

export default App;
